-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2019. Jún 14. 19:01
-- Kiszolgáló verziója: 10.1.29-MariaDB
-- PHP verzió: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `laravel_workportal`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Laravel jobs', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(2, 'WordPress jobs', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(3, 'CakePHP jobs', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(4, 'Symfony jobs', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(5, 'Zend Framework jobs', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(6, 'CustomPHP jobs', '2019-05-29 04:24:44', '2019-05-29 04:24:44');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `cname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `companies`
--

INSERT INTO `companies` (`id`, `user_id`, `cname`, `slug`, `address`, `phone`, `website`, `logo`, `cover_photo`, `slogan`, `description`, `created_at`, `updated_at`) VALUES
(1, 14, 'Smitham, Gleason and Lockman', 'smitham-gleason-and-lockman', '2990 Adelle Park\nWest Lon, NC 77619', '+1.528.468.6641', 'jaskolski.com', 'ceb462936b2962b7ab3793c596e0b1c0.jpg', '1ece295aee0bfb992cf38cb2eab4a972.jpg', 'lorem ipsum', 'Saepe autem recusandae dolorem aut amet et quo neque. Esse voluptatem dolorem in labore rerum eveniet distinctio. Sed quia doloremque ut consequatur eius unde odio. Odio magni non autem sed s', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(2, 18, 'Marks Ltd', 'marks-ltd', '725 Quigley Crossing\nNorth Okey, CA 98724', '+1.252.603.3245', 'harber.info', 'a2c932e3cc69fd8bcab74c3d3e85c368.jpg', '69639346f8348e0bb616f316c8eab8ad.jpg', 'lorem ipsum', 'Assumenda porro dolorem quisquam necessitatibus tempora. Sit fuga vero ut eum alias atque ipsam. Recusandae ratione ut impedit dolor dolor recusandae. Alias necessitatibus quod explicabo face', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(3, 17, 'Mayer Inc', 'mayer-inc', '83050 Everett Inlet\nPort Carlo, FL 23385-2512', '945-669-8693', 'harber.info', '7d3e009b3d24ba896abd1f5f2147cb40.jpg', 'b2e110bcd24ae4ad5c07ed1f1cf1d9c6.jpg', 'lorem ipsum', 'Consectetur dolores porro quidem iusto ratione cum optio. Nihil sit enim quis repellat. Perspiciatis aliquam reiciendis sunt atque repellat est pariatur. Eaque consequatur maxime necessitatib', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(4, 13, 'Bailey-Lueilwitz', 'bailey-lueilwitz', '5485 Mills Island\nNew Madisen, AL 48815', '892.278.7464 x39613', 'beer.com', 'b8df0914ed058593c19d3fe5a9fb7753.jpg', 'a70ca2f7fb5bcfe67f623ad76e1cfbc6.jpg', 'lorem ipsum', 'Doloremque et non odit veniam sint molestiae. Excepturi sunt sit nihil et enim in. Quaerat quia consectetur possimus est sapiente cupiditate sit. Similique quibusdam inventore vel est modi.', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(5, 11, 'Leannon, Klocko and Little', 'leannon-klocko-and-little', '75865 Natasha Radial\nSofiaberg, MO 48543', '1-834-484-5583 x55704', 'hauck.com', '2d0aec5082d028d0293b9ee5b23da267.jpg', '4b82966da708e91cfb2e18589a08246a.jpg', 'lorem ipsum', 'Quae cupiditate ipsa nam aperiam facilis dolores. Dolorum ut vero rem explicabo sint. Cumque expedita nam non omnis est rem quas. Ut aut tempora et error corrupti quas corporis. Eligendi quo ', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(6, 18, 'Schaefer-Hartmann', 'schaefer-hartmann', '94175 Klocko Wall\nEast Stevie, NV 01592-5978', '1-860-931-9298 x83617', 'sipes.com', '5803641d9002d0913cf8802826de1c10.jpg', '008bd6d8492ce1e97ed628d22a53e6b6.jpg', 'lorem ipsum', 'Neque vel distinctio nesciunt velit cum ut. Molestias illum vel sit earum labore nostrum sunt modi. Mollitia reprehenderit quam alias.', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(7, 20, 'Feest LLC', 'feest-llc', '280 Dayana Spur Apt. 645\nEdwardochester, CT 07376', '461.479.4373 x51756', 'beahan.info', 'e1d6ebcdadb7b9d8089daba02a1a6044.jpg', '4a731d37ab4df4086e1862388bbcc69e.jpg', 'lorem ipsum', 'Ut provident sit aut deleniti. Tempora id enim assumenda amet omnis voluptas neque. Necessitatibus iure dolorem accusantium adipisci ratione in. Nisi veniam minima qui quo nesciunt totam aut ', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(8, 18, 'Halvorson Inc', 'halvorson-inc', '5005 Liam Glens\nLake Bethel, ID 10847', '1-427-746-9176 x57037', 'legros.biz', '451122091041ee39bde55da3cb7dffd5.jpg', '7ad8891fcfe3d74fbc452738097488a5.jpg', 'lorem ipsum', 'Perspiciatis laborum cumque ratione molestiae non vel cum cum. Consequatur quibusdam quod fugiat autem. Quisquam optio et autem. Aut nisi qui consequatur sunt consequatur nihil sit voluptatum', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(9, 2, 'Schneider LLC', 'schneider-llc', '3585 Hansen Port\nSouth Mariahville, SC 83539', '1-335-966-0432 x22708', 'cole.com', '8eb874d7e0767f40682586bb138d7c85.jpg', '0678c999dbf85c7c5ed657da60465355.jpg', 'lorem ipsum', 'Est voluptas blanditiis illum error perspiciatis delectus facilis. Cum beatae eligendi non et odio sequi. Cupiditate ut quia eum. Maxime pariatur error tempore quaerat incidunt in maxime. Sit', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(10, 1, 'Bechtelar PLC', 'bechtelar-plc', '601 Mittie Locks\nNorth Salma, SD 17691', '(834) 614-0367', 'predovic.com', '13ebc926066cd9a362098bb800ecf620.jpg', '84966d9031dbe48fdb61efb8a82b257a.jpg', 'lorem ipsum', 'Esse maiores nemo est quia eaque. Sunt dicta ratione ipsum omnis cum magni omnis. Et aut est sequi vero molestiae quos quia. Qui perferendis vel magnam rerum et. Consequatur non magni volupta', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(11, 12, 'Kling-Towne', 'kling-towne', '71495 Katlynn Islands\nWest Kole, ME 22077', '681.453.6209', 'mcglynn.com', '7154f5210e2ee20f52dd5ec3d5bb2a26.jpg', '79c0a84b4cba1655840151227621f929.jpg', 'lorem ipsum', 'Magni id ipsum suscipit sit id. Enim delectus iste delectus eligendi vel ullam molestiae commodi. Cumque quo nihil alias. Perspiciatis possimus doloremque libero velit autem autem. Sed repudi', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(12, 14, 'Connelly-Dare', 'connelly-dare', '8506 Weissnat Vista\nWest Marquestown, WA 94054-5894', '980-752-2774', 'sporer.org', 'b862e549038e3bd78eb85143553987b8.jpg', 'caf8a9b9586fca3cce6e50f3812b1603.jpg', 'lorem ipsum', 'Fugit sequi voluptate temporibus deserunt. Nihil veniam sed odio laboriosam. Reprehenderit voluptatem molestiae et nobis. Corrupti asperiores dicta quia fugiat. Eligendi quis neque deleniti n', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(13, 2, 'Terry, Schumm and Bernhard', 'terry-schumm-and-bernhard', '5794 Elvie Squares\nWest Jillian, NC 39313-9249', '307.451.0638', 'kuhic.com', 'd3e21825c238d99eedc5328c511221b0.jpg', '982843b2b24e9ed149771560bf26f115.jpg', 'lorem ipsum', 'Iste nihil aliquid nesciunt. Provident a voluptatem ab soluta id eum.', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(14, 11, 'Toy-Bayer', 'toy-bayer', '7431 Hermann Fields Apt. 992\nBlickburgh, NY 06346-4455', '(861) 988-4756 x6708', 'metz.com', 'ed97675ec89b9377f2dba4296361980e.jpg', '399463515b0f473543c01b3511c9e01e.jpg', 'lorem ipsum', 'Eveniet cupiditate velit aspernatur sint vel. Consequatur maiores cupiditate recusandae. Et sed consequatur quas exercitationem et consequatur. Vero omnis dignissimos qui provident ut quos cu', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(15, 18, 'Beier, Daniel and Collier', 'beier-daniel-and-collier', '6882 Karelle Fort\nEmelieshire, PA 52499', '814.213.8966', 'jacobs.com', '873b5d5a9eadbfc60684ad5e29c86412.jpg', 'd7cb3a1666397ed45058728266f43bd6.jpg', 'lorem ipsum', 'Unde id consequuntur qui facilis atque qui aspernatur id. Fugiat ducimus asperiores ratione dolorem corporis consectetur necessitatibus et. Laborum sunt dolorem rerum libero. Facilis necessit', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(16, 13, 'Terry and Sons', 'terry-and-sons', '4908 Lorenz Forest\nBridgetteburgh, MN 60728', '785-787-6549 x8037', 'rohan.org', '4658633d67a6330b2a120d4a9f804534.jpg', '8df333458d83009c44a5d16bef245eb3.jpg', 'lorem ipsum', 'Odio corporis quos laborum. Est ab et recusandae minus quaerat. Voluptatem cum autem et aliquid. Ab quis voluptas voluptas ut. Accusantium natus dolor et voluptates eveniet quas repudiandae. ', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(17, 2, 'Anderson PLC', 'anderson-plc', '1652 Quigley Trail\nSawaynfort, MI 85662', '643.951.7853', 'mertz.com', '014031d1ccdc7e47c368cadb9a312844.jpg', '3bc5364a720e82dc0213d8252537d5db.jpg', 'lorem ipsum', 'Commodi veritatis aut culpa dicta cum. Sequi doloremque omnis vel minus. Ea corrupti quia qui illum earum consectetur similique autem. Repudiandae ducimus rerum repellat iste. Fugiat est reic', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(18, 7, 'Cummerata Group', 'cummerata-group', '159 Romaguera Pines Suite 738\nSouth Destinymouth, LA 45885', '1-756-363-0294 x52244', 'aufderhar.com', 'aa3eeb05e9759d574de43f93c2fd7284.jpg', '9409954d9fd38d5ca10a573294fc9414.jpg', 'lorem ipsum', 'Cum quaerat a qui ut recusandae non. Quaerat quas quod doloribus dolor eum iusto nostrum culpa. Sit error ipsum inventore voluptas quia cupiditate. Soluta in error et tempore eius quo sit. Vo', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(19, 6, 'Rau PLC', 'rau-plc', '4599 Mraz Trail Suite 898\nEast Fay, TX 50179', '254-942-7673', 'fritsch.com', '9dc8718271161ad94d2d8695a7c7f0df.jpg', '9b396125715319e2249edf3281d0e848.jpg', 'lorem ipsum', 'Vero dolore quis animi. Quod nihil placeat maxime placeat beatae. Non libero quas sed dignissimos. Veritatis officiis amet quia dicta ad assumenda cum. Omnis et ut expedita explicabo sed ut a', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(20, 19, 'Haag-Cartwright', 'haag-cartwright', '33215 Haag Road Apt. 500\nAlbertohaven, GA 92950', '416.894.2503 x8616', 'rowe.biz', 'ff11edf05e4ec9a001a06dea309152bb.jpg', '8494f6aa16533ca89415ea0ba6bf2b6c.jpg', 'lorem ipsum', 'Deleniti vel incidunt et quam beatae minima. Quis libero qui quisquam aut quae ut aliquid. Omnis dolores tempora qui fugit culpa similique. Atque eligendi perferendis sed sit excepturi. Quis ', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(21, 22, 'MicroCode', 'microcode', 'Budapest', '67567657', 'wwwwww', '1559294253.jpg', '1559298220.jpg', 'gdfgdfg', 'fdgdfgdf', '2019-05-31 05:46:33', '2019-05-31 08:24:31');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `favourites`
--

CREATE TABLE `favourites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `favourites`
--

INSERT INTO `favourites` (`id`, `job_id`, `user_id`, `created_at`, `updated_at`) VALUES
(10, 3, 21, '2019-06-12 17:37:51', '2019-06-12 17:37:51'),
(11, 4, 21, '2019-06-12 17:40:01', '2019-06-12 17:40:01');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `last_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `jobs`
--

INSERT INTO `jobs` (`id`, `user_id`, `company_id`, `title`, `slug`, `description`, `roles`, `category_id`, `position`, `address`, `type`, `status`, `last_date`, `created_at`, `updated_at`) VALUES
(1, '5', '1', 'Adipisci doloremque velit dolor error magni sed quia. Autem rerum dolores necessitatibus sint. Vero pariatur aspernatur omnis dicta neque quis hic. Facilis impedit et suscipit sit ex officiis', 'adipisci-doloremque-velit-dolor-error-magni-sed-quia-autem-rerum-dolores-necessitatibus-sint-vero-pariatur-aspernatur-omnis-dicta-neque-quis-hic-facilis-impedit-et-suscipit-sit-ex-officiis-di', 'Eveniet rerum sunt qui laudantium. Quisquam quod perspiciatis qui repellat mollitia. Facere assumenda temporibus atque aut. Voluptatem quam aut quia blanditiis. Assumenda blanditiis reprehenderit totam dolores atque eum impedit. Culpa et et quia sapiente voluptas commodi in. Eius eveniet rem earum et et. Reiciendis explicabo est veritatis quaerat saepe qui incidunt. Praesentium aliquid maxime aut in.', 'Aliquam est ut velit. Deleniti nam soluta voluptate quia autem et. Iusto ex enim aut sunt. Quos repudiandae rem ut ipsam consectetur rerum vel.', 1, 'Loan Counselor', '4804 McClure Corner\nNorth Hunterhaven, WA 12650', 'fulltime', 1, '1996-05-12', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(2, '7', '11', 'Sunt est officia et molestiae. Repellendus velit expedita iste dolorum.', 'sunt-est-officia-et-molestiae-repellendus-velit-expedita-iste-dolorum', 'Consequatur possimus harum voluptate autem tempora adipisci maiores. Qui dolorem sed cum laudantium totam est. Minus et fuga dolores est eveniet.', 'Ducimus atque rerum ut provident suscipit nam voluptatibus. Architecto corrupti quia consequatur ducimus labore. Ad sed sit iste aliquid.', 3, 'Network Systems Analyst', '20976 Keeling Creek Suite 527\nRuntehaven, AR 72578', 'fulltime', 1, '1981-02-20', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(3, '13', '20', 'Ipsam a dicta sit. Quo saepe praesentium nihil placeat minus itaque. Et voluptatem excepturi consequatur dolores eveniet qui id.', 'ipsam-a-dicta-sit-quo-saepe-praesentium-nihil-placeat-minus-itaque-et-voluptatem-excepturi-consequatur-dolores-eveniet-qui-id', 'Rerum iusto necessitatibus harum accusamus qui qui ut. Maxime voluptas nisi repellat qui. Ea ratione vel et magni id perferendis necessitatibus. Eos odit consectetur labore aspernatur est. Ad a officia non dolore dolor qui. Iusto pariatur eos laudantium vitae. Quis repudiandae nobis nihil aut nisi non provident. Enim aliquam voluptatibus dicta. Repellat earum voluptas eos debitis. Accusamus pariatur iusto velit quasi sed nostrum. Dolorem vero eos architecto molestiae suscipit iste laboriosam.', 'Voluptas et ut quis neque. Ut a placeat enim velit eum corrupti. Excepturi ut consequatur veritatis laborum.', 2, 'Heating and Air Conditioning Mechanic', '5469 Kulas Lodge Suite 271\nFridaberg, WA 58215', 'fulltime', 1, '1988-06-21', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(4, '10', '5', 'Numquam sit officiis eaque ut. Aut et laudantium reiciendis molestiae. Doloremque libero assumenda repellat omnis repellendus illo modi. Qui eligendi architecto ex deleniti dolores velit cons', 'numquam-sit-officiis-eaque-ut-aut-et-laudantium-reiciendis-molestiae-doloremque-libero-assumenda-repellat-omnis-repellendus-illo-modi-qui-eligendi-architecto-ex-deleniti-dolores-velit-consequ', 'Molestias laudantium minima non similique doloremque quam in veritatis. Et ad enim officiis ipsum assumenda nisi. Sed nisi libero et corporis consequatur culpa est sed. Occaecati aut neque itaque quia. Voluptatem quis beatae consequatur molestiae.', 'Sapiente ratione aperiam enim quo cupiditate. Id asperiores et dolores odit dolorem sit enim. Quisquam cum quae fugit assumenda. Et id odit facere similique. Nihil et voluptatem repellat.', 2, 'Farmer', '65198 Breitenberg Via Apt. 404\nConroyville, NC 42169-6621', 'fulltime', 1, '2001-09-12', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(5, '19', '18', 'Impedit ipsum cupiditate sint voluptate. Recusandae odio quibusdam amet doloribus odio vitae. Quis eos voluptate repellendus impedit error. Velit quasi recusandae molestias molestias.', 'impedit-ipsum-cupiditate-sint-voluptate-recusandae-odio-quibusdam-amet-doloribus-odio-vitae-quis-eos-voluptate-repellendus-impedit-error-velit-quasi-recusandae-molestias-molestias', 'Esse in esse voluptate possimus dignissimos laborum. Esse quia quia beatae eius officia voluptate. Placeat iste sunt eos qui.', 'Sequi officia sit aliquid est nihil occaecati sed iusto. Rem fugit inventore saepe ea. Maiores earum vel molestiae.', 2, 'Furniture Finisher', '493 Morar Corners Suite 693\nEast Else, MA 76738', 'fulltime', 0, '1973-02-08', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(6, '20', '15', 'Nesciunt qui voluptates eum consequatur et aspernatur facilis. Aut est sit ut.', 'nesciunt-qui-voluptates-eum-consequatur-et-aspernatur-facilis-aut-est-sit-ut', 'Officia corporis eius iure. Tenetur non blanditiis modi minus veritatis illum voluptatem. Eos cum aperiam eos a et.', 'Et quia asperiores enim quidem tempora esse. Reiciendis expedita autem eius aut. Cum dolorum est error minima qui cumque est id.', 3, 'Food Preparation Worker', '95702 Luella Lakes\nWest Ivory, OR 21209', 'fulltime', 0, '1980-01-01', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(7, '4', '9', 'Consequuntur voluptatem voluptatibus voluptates nisi. Magnam consequatur quam aliquam totam. Quibusdam mollitia architecto architecto pariatur veritatis quasi. Velit et corporis ea sunt rerum', 'consequuntur-voluptatem-voluptatibus-voluptates-nisi-magnam-consequatur-quam-aliquam-totam-quibusdam-mollitia-architecto-architecto-pariatur-veritatis-quasi-velit-et-corporis-ea-sunt-rerum-be', 'Iste nam enim fugiat ullam. Voluptatem reprehenderit aut enim vero dolorem. Unde perferendis eos ut sed commodi. Blanditiis a deleniti vitae et.', 'Ex libero omnis unde ut. Id corporis assumenda eligendi id blanditiis. Dignissimos ipsum iste et eum. Vel accusantium minus blanditiis vitae adipisci vel excepturi asperiores.', 3, 'Rough Carpenter', '17413 Kaitlyn Ford\nPascaleberg, OH 59141', 'fulltime', 1, '2000-07-18', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(8, '8', '12', 'Et voluptatem ut fugit perferendis atque. Esse pariatur dolor nulla. Cumque qui quis sit dolor ab delectus praesentium.', 'et-voluptatem-ut-fugit-perferendis-atque-esse-pariatur-dolor-nulla-cumque-qui-quis-sit-dolor-ab-delectus-praesentium', 'Libero aspernatur expedita dolor excepturi porro sit ad. Soluta doloribus sed totam explicabo ipsa est reprehenderit aliquid. Ullam dolores laudantium et excepturi accusantium sunt. Doloribus rerum odio quia eos. Et odit rerum non minima omnis. Molestiae vitae inventore minus sint sint consequatur.', 'Illo est id similique aspernatur asperiores incidunt. Quod deleniti aut ex omnis. Tempore quo blanditiis velit et dolore ipsum.', 3, 'Safety Engineer', '365 Germaine Lock Suite 948\nPort Litzy, NH 80217', 'fulltime', 1, '1981-04-26', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(9, '8', '7', 'Vero veniam voluptatibus est est. Rerum eos id et odio. Est exercitationem excepturi tempora ut.', 'vero-veniam-voluptatibus-est-est-rerum-eos-id-et-odio-est-exercitationem-excepturi-tempora-ut', 'Iusto cupiditate sint ut dolor. Hic quo porro quibusdam minus voluptates rem error ad. Tempora minima quis est natus aperiam dignissimos. Doloribus cupiditate nihil voluptatem odio sint. Id deleniti culpa qui tempore eligendi. Voluptatem occaecati et vitae eos dolor. Asperiores ipsa est dolorem rerum ipsum. Qui quis eos id alias tenetur reprehenderit dolorem.', 'Numquam tempora ut ad fugit modi eos hic. Et vel molestiae a sunt autem maiores. Suscipit et atque sed ut fugit ex corporis. Ipsa et sit omnis debitis sunt delectus itaque.', 5, 'Computer Security Specialist', '76132 Nicolette Isle\nSylvanport, CT 40270', 'fulltime', 0, '2018-06-14', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(10, '11', '16', 'Cum rerum voluptatem facere doloremque. Consequatur distinctio qui omnis eius. Laboriosam et voluptatibus numquam sequi quaerat nulla. Assumenda sunt ut laboriosam eligendi sapiente.', 'cum-rerum-voluptatem-facere-doloremque-consequatur-distinctio-qui-omnis-eius-laboriosam-et-voluptatibus-numquam-sequi-quaerat-nulla-assumenda-sunt-ut-laboriosam-eligendi-sapiente', 'Facilis quasi consequatur quaerat minus. Et commodi dolore magni iusto quae ea. Expedita vel quia repudiandae nobis quia sunt nam fugit. Aut itaque aspernatur et excepturi.', 'In veritatis ut ab ipsa maiores consequatur. Vel facere nisi doloremque explicabo. Sapiente non dolore assumenda. Maxime sit non hic optio fuga vero delectus. Quia quam facere mollitia est.', 1, 'Architectural Drafter OR Civil Drafter', '473 Williamson Springs\nEast Juneport, RI 83748-8956', 'fulltime', 1, '1971-11-17', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(11, '15', '3', 'Et et et quibusdam. Aut eveniet necessitatibus amet eius. Quia reiciendis ullam ut ipsum possimus. Omnis vitae ex eligendi voluptas.', 'et-et-et-quibusdam-aut-eveniet-necessitatibus-amet-eius-quia-reiciendis-ullam-ut-ipsum-possimus-omnis-vitae-ex-eligendi-voluptas', 'Ratione quam recusandae enim et nesciunt ut ut. Excepturi voluptas eos ut voluptates laboriosam accusantium illo. Officia quia ut laboriosam totam. Ipsa fugit dolore sed. Vel non quasi sequi ex.', 'Nulla est eaque excepturi earum. Rerum occaecati deserunt et culpa.', 5, 'Home Appliance Installer', '9030 Trycia Trail Apt. 420\nNew Monserrat, MT 64061', 'fulltime', 0, '1983-12-28', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(12, '11', '20', 'Quam fugit voluptas qui fuga aut. Officia earum enim cum quia ut est libero. Et voluptas assumenda nemo accusantium nihil dolores. Ut omnis et porro autem est et.', 'quam-fugit-voluptas-qui-fuga-aut-officia-earum-enim-cum-quia-ut-est-libero-et-voluptas-assumenda-nemo-accusantium-nihil-dolores-ut-omnis-et-porro-autem-est-et', 'Doloremque dicta assumenda qui suscipit. Architecto quisquam aperiam est quam. Temporibus dolore velit nulla blanditiis ut.', 'Ullam perspiciatis ut odit ipsa voluptatem quia a quidem. Delectus et tempore unde nesciunt quas. Et magnam id reiciendis eos ex. Eveniet maxime voluptatem molestiae possimus sed.', 5, 'Psychology Teacher', '27776 Hand Crossing\nDaughertybury, PA 47797', 'fulltime', 1, '2015-12-29', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(13, '4', '6', 'Optio rerum unde ducimus molestiae iste. Tenetur culpa doloremque rerum animi adipisci ex. Porro est qui quia et. Soluta repellendus mollitia consequatur tenetur harum laborum quam.', 'optio-rerum-unde-ducimus-molestiae-iste-tenetur-culpa-doloremque-rerum-animi-adipisci-ex-porro-est-qui-quia-et-soluta-repellendus-mollitia-consequatur-tenetur-harum-laborum-quam', 'Aut ea suscipit in esse fugiat eligendi. Sit est nulla deserunt facere sapiente voluptas qui. Explicabo animi autem eveniet laboriosam molestiae non placeat. Expedita eum nulla quis ullam quo in autem sit. Officiis vitae non quidem consequatur. Repellat aut quia aut vitae fugit tempora tempora voluptatum. Repudiandae saepe quos error. Fugit amet eos et illo quidem nam. Et magni necessitatibus neque alias libero qui. In ut officiis amet minima similique distinctio libero consequatur.', 'Quod pariatur in maiores optio quas nisi accusantium. Quos totam qui quod voluptatem est distinctio. Non officiis molestias vel eos ab quidem porro. Unde eum quia molestiae repudiandae.', 5, 'Agricultural Worker', '6667 Moore Village\nNorth Joshmouth, MI 91853-9211', 'fulltime', 1, '1982-06-05', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(14, '12', '17', 'Adipisci nesciunt blanditiis sed facilis illum consequatur nulla. Mollitia exercitationem eos suscipit assumenda ea. Aut fugiat aliquid nisi.', 'adipisci-nesciunt-blanditiis-sed-facilis-illum-consequatur-nulla-mollitia-exercitationem-eos-suscipit-assumenda-ea-aut-fugiat-aliquid-nisi', 'Sit dolores aspernatur veniam sapiente fuga. Laborum id sed provident est earum. Harum numquam qui quis aperiam vitae aut aut. Commodi in est atque odit ut. Necessitatibus rerum suscipit repudiandae. Tempora et suscipit tempore modi et ipsam sequi. Corporis repellat similique neque vel quo autem.', 'Dolores voluptatum error consequuntur qui quam rerum. Autem consequuntur occaecati minus nihil rerum ea. Excepturi repudiandae nihil et ducimus quis reprehenderit.', 4, 'Precision Devices Inspector', '8669 Larissa Mill Suite 599\nKuphalview, KS 51209', 'fulltime', 1, '1987-12-09', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(15, '20', '8', 'Occaecati cum fugit est amet alias itaque delectus ut. Rerum molestiae quae eum. Et accusantium sit eligendi et omnis molestias. Omnis nostrum hic laborum repellat.', 'occaecati-cum-fugit-est-amet-alias-itaque-delectus-ut-rerum-molestiae-quae-eum-et-accusantium-sit-eligendi-et-omnis-molestias-omnis-nostrum-hic-laborum-repellat', 'Maxime in eligendi qui sed sequi voluptatem voluptas. Qui laborum distinctio sint harum exercitationem nesciunt possimus. Quam rem accusantium asperiores esse totam. Ratione ut molestiae quas laborum quasi. Facere occaecati dolorem quia animi aut. Est libero et ab placeat. Quo odit quo sint omnis ut amet. Voluptas et nesciunt dignissimos possimus dolorem. Est architecto quas voluptatibus odit ipsam fugiat iste. Officia illo in et. Animi libero quas at ex nesciunt laborum qui.', 'Fuga at ut officia magnam. Laborum asperiores nobis voluptates nostrum adipisci consectetur. Dignissimos optio deserunt omnis veniam amet.', 2, 'Precision Instrument Repairer', '506 West Lake\nHuelville, IL 47349', 'fulltime', 1, '2008-09-17', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(16, '13', '15', 'Ut aperiam quae quia omnis. Sunt cum sit non itaque est. Quis alias nihil eos mollitia sit possimus rem.', 'ut-aperiam-quae-quia-omnis-sunt-cum-sit-non-itaque-est-quis-alias-nihil-eos-mollitia-sit-possimus-rem', 'Non minus atque vel et voluptas doloribus amet. Quisquam eaque explicabo dolorem quasi. Sit unde architecto qui harum id vel laudantium praesentium. Accusantium et optio sint impedit et aspernatur. Ipsa voluptatem sapiente dolore doloremque ab neque illo. Quos sint quis sunt excepturi vel tempore.', 'Est dolorum est minima totam quia. Natus quia qui delectus laborum dolores. Et quis non et quia natus inventore nobis omnis.', 3, 'Outdoor Power Equipment Mechanic', '800 Oceane Streets\nPearlineville, NM 15336', 'fulltime', 1, '1984-11-14', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(17, '19', '7', 'Voluptas illo qui quas distinctio in. Dolores veritatis et earum id vel reiciendis. Commodi id ea quos assumenda est consequatur. Et facilis eum vel qui dicta aut ullam itaque.', 'voluptas-illo-qui-quas-distinctio-in-dolores-veritatis-et-earum-id-vel-reiciendis-commodi-id-ea-quos-assumenda-est-consequatur-et-facilis-eum-vel-qui-dicta-aut-ullam-itaque', 'Qui ab nobis pariatur velit similique neque. Maxime dolores accusantium aut autem non cupiditate. Eum dolorem quos tenetur voluptas maiores aut.', 'Velit sapiente quam iure architecto. Quo veniam voluptates suscipit laudantium omnis et assumenda. Ipsam reiciendis blanditiis maiores voluptate aut.', 2, 'Library Assistant', '11366 Goldner Walk Apt. 068\nPort Lester, OH 80800', 'fulltime', 1, '1982-11-07', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(18, '14', '6', 'Culpa qui reprehenderit voluptatem. Libero amet beatae molestiae aut. Ex quibusdam ipsa id voluptates voluptas fuga. Eos aspernatur nihil fugit perspiciatis mollitia.', 'culpa-qui-reprehenderit-voluptatem-libero-amet-beatae-molestiae-aut-ex-quibusdam-ipsa-id-voluptates-voluptas-fuga-eos-aspernatur-nihil-fugit-perspiciatis-mollitia', 'Ea unde atque qui nihil. Praesentium et illo est itaque blanditiis est. Numquam aliquid ex veniam commodi neque. Non voluptas vitae est sunt occaecati. Porro numquam laborum magnam autem architecto nemo. Laboriosam quibusdam reiciendis voluptatem rem vel et. Consectetur rerum facilis tempore sapiente hic. Dolores hic sit vero dolorum temporibus doloremque. Cumque expedita earum quis commodi.', 'Ut voluptatibus sit error ex eum. Placeat voluptate ut aut distinctio nostrum cumque maxime. Eum quo aut consequatur aspernatur temporibus nostrum.', 3, 'Podiatrist', '4104 Trever Knoll\nLake Jeffreyfort, NM 65103-1728', 'fulltime', 1, '1974-07-30', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(19, '11', '7', 'Quidem dolorum et ullam quis in. Voluptas accusantium saepe consequuntur ut qui voluptatem modi. In sint voluptatem velit dolorem dolor dignissimos eos. Soluta soluta dolor et ea dignissimos ', 'quidem-dolorum-et-ullam-quis-in-voluptas-accusantium-saepe-consequuntur-ut-qui-voluptatem-modi-in-sint-voluptatem-velit-dolorem-dolor-dignissimos-eos-soluta-soluta-dolor-et-ea-dignissimos-sun', 'Eveniet qui asperiores nihil nesciunt. Explicabo molestias reprehenderit molestias voluptatem ad vitae vel. Dignissimos rerum iste est quibusdam velit porro. Cumque et dolore repudiandae alias perferendis similique. Exercitationem ex aut amet perspiciatis eveniet. Consequuntur molestiae voluptatum vero sit soluta beatae odio.', 'Magnam quasi veritatis quibusdam ex. Mollitia saepe mollitia sed eligendi exercitationem. Veritatis nihil perspiciatis nostrum.', 2, 'Roofer', '6452 Prohaska Lodge Apt. 566\nNew Julieshire, NE 33076-6480', 'fulltime', 0, '2013-08-13', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(20, '2', '12', 'Tenetur impedit similique aut. Et inventore architecto a vero. Molestiae saepe aut exercitationem saepe eos doloribus deserunt. Molestias vel sit iusto cum itaque temporibus omnis aliquid.', 'tenetur-impedit-similique-aut-et-inventore-architecto-a-vero-molestiae-saepe-aut-exercitationem-saepe-eos-doloribus-deserunt-molestias-vel-sit-iusto-cum-itaque-temporibus-omnis-aliquid', 'Exercitationem consequatur est autem aut. Laudantium ut placeat fugiat saepe placeat magnam. Et id consequatur provident modi officiis voluptas id. Quidem eveniet sunt numquam dicta expedita enim.', 'Omnis molestiae earum consectetur. Non ea qui nihil sequi fugit ipsam. Nulla hic soluta magni officia nobis eum. Similique eos qui nulla. In eos hic at. Molestiae blanditiis voluptas autem delectus.', 1, 'Automotive Technician', '45804 Kub Isle Apt. 543\nEast Lennaport, AK 51436', 'fulltime', 1, '1972-02-12', '2019-05-29 04:24:44', '2019-05-29 04:24:44'),
(21, '22', '21', 'Laravel developer', 'laravel-developer', 'Expreience 5 year', 'dev', 6, 'web developer 1', 'Melbourbnne', 'fulltime', 1, '0000-00-00', '2019-05-31 10:38:36', '2019-05-31 10:38:36');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `job_user`
--

CREATE TABLE `job_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `job_user`
--

INSERT INTO `job_user` (`id`, `job_id`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 21, 21, '2019-06-05 09:40:06', '2019-06-05 09:40:06'),
(4, 2, 21, '2019-06-07 18:34:20', '2019-06-07 18:34:20'),
(5, 8, 21, '2019-06-07 19:00:14', '2019-06-07 19:00:14'),
(6, 3, 21, '2019-06-11 08:01:49', '2019-06-11 08:01:49'),
(7, 1, 21, '2019-06-12 17:43:53', '2019-06-12 17:43:53'),
(8, 13, 21, '2019-06-12 17:44:29', '2019-06-12 17:44:29');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(73, '2014_10_12_000000_create_users_table', 1),
(74, '2014_10_12_100000_create_password_resets_table', 1),
(75, '2019_04_18_082128_create_profiles_table', 1),
(76, '2019_04_18_082221_create_companies_table', 1),
(77, '2019_04_18_082318_create_categories_table', 1),
(78, '2019_04_18_082510_create_jobs_table', 1),
(79, '2019_04_18_082843_create_job_user_table', 1),
(80, '2019_04_18_082948_create_favourites_table', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_letter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `address`, `gender`, `dob`, `experience`, `bio`, `cover_letter`, `resume`, `avatar`, `created_at`, `updated_at`) VALUES
(1, '21', '', 'male', '05/01/2000', '', '', 'public/files/QCnLhpw2YOEX4QxXgMyxlN4aFxpw3TyFzCmO2pKO.jpeg', 'public/files/KpYRJznygwVq9BUEpJtKwjinUSzSF28zl9lziHpc.jpeg', 'avatar_1559294795.jpg', '2019-05-29 04:39:55', '2019-05-31 10:22:01');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `user_type`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mrs. Melissa Schultz', 'seeker', 'arianna23@example.net', '2019-05-29 04:24:33', '$2y$10$qP0OcEwPm7Pik1kxrYTiROmH8LRSLzXhQbzBhF5DcOYupVcRDyKJC', 'WO88fWZmdV0DurS27Fv6k17BMByUV2PmIhcTYIrg1WSj7UjApqVmB1MmCTlU', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(2, 'Jaida Turcotte', 'seeker', 'regan.erdman@example.net', '2019-05-29 04:24:33', '$2y$10$eAamy46bwiOZ/2cacw7OKuPFBJAsXUK0Zc/i0JBBzhAwkS8GmRmju', 'Um13pR7Shz', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(3, 'Dr. Lilyan Bins DVM', 'seeker', 'turcotte.lexi@example.org', '2019-05-29 04:24:33', '$2y$10$qSXIoYFunKr.CR5rnhdGh.DelrIzh/LzLCfDK04drW78XP4U3dDr.', 'M6FMVHzrKg', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(4, 'Lucas Kautzer', 'seeker', 'mhuel@example.org', '2019-05-29 04:24:33', '$2y$10$m5myuK86RKOsRiNISEMa8OZ1NwpAH9JI5JkMBmgVHqnHndzcYmhFu', '85spCjWjhR', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(5, 'Erika Gerhold', 'seeker', 'charles51@example.net', '2019-05-29 04:24:33', '$2y$10$KPexXXtyhpmJ6R8JVl32LOM0QJVG0cd4GEj5YH.QE5o/UbVhCY7f.', '2Xw33Ac3qq', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(6, 'Marc Gislason', 'seeker', 'edwina.ondricka@example.net', '2019-05-29 04:24:34', '$2y$10$2MMm2iVzfgjaeq890Tc92OP6ASdhgzx.2/BZKsbUD94xW5pe32KZa', 'FNVhtVwjRE', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(7, 'Dr. Timmy Prosacco', 'seeker', 'dewayne16@example.net', '2019-05-29 04:24:34', '$2y$10$DspVDIIjIHW8ZWOpi6N4huV.PKIJNL7N0KuonVW8C1l2LD6rMTjx2', 'W5kLipIA2t', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(8, 'Mrs. Brandyn Hand', 'seeker', 'schimmel.lexie@example.net', '2019-05-29 04:24:34', '$2y$10$UrqCamnuEdUjCg8blnVmB.QJrS2UdWyh2ngZ/mApllwJh6v9S04Zq', 'yRS5mBb3QC', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(9, 'Lillie Breitenberg', 'seeker', 'maegan46@example.net', '2019-05-29 04:24:34', '$2y$10$jfvbkV2fCLd.hhzDuf4L5OQ/lL1TjSCBPIyEmahwRol/DC8dsEa66', 'pzUbVPhfQG', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(10, 'Prof. Horacio Toy Jr.', 'seeker', 'chanelle.shanahan@example.net', '2019-05-29 04:24:34', '$2y$10$jo3N0OKl7Q62IPVzoZE38eXhKL1/RbUoqkGV9YciwYXKyGHJFDa8q', 'JrSfRZ95lI', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(11, 'Eloisa Medhurst', 'seeker', 'raul.gerlach@example.org', '2019-05-29 04:24:34', '$2y$10$YsQ.RDE7KaxPmhTc62mZEOhK5Hyh5npvfNNDMzSFzfqW8bemseHLe', 'YD1wFtLQ6y', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(12, 'Leilani Carroll', 'seeker', 'gunnar39@example.com', '2019-05-29 04:24:34', '$2y$10$rVxPLjoWlFqKbLIn5dJzSufqovI..Wy0eqVxVpHQ5kRYqifsOCsTy', 'ULRvai9zqS', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(13, 'Vidal Brown', 'seeker', 'emerson.daugherty@example.com', '2019-05-29 04:24:34', '$2y$10$cmiwpP/mHDur1ZfxetaeN.oB4NVdBvSEIgnttKmpGgXOpW8baN0a6', 'Aou7t83UZx', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(14, 'Krystal Bode', 'seeker', 'beer.felipe@example.org', '2019-05-29 04:24:34', '$2y$10$jlF92JRnkqX3u0zlG44f/eNYJht9rXPPI1FyNF8dTmmX61PTTZjia', 'wNKfZYls98', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(15, 'Gregory Yost', 'seeker', 'qwehner@example.com', '2019-05-29 04:24:34', '$2y$10$ZpCGGB4icCyfcjnL5X8NWu5vy/kHO1H3n.YmpE92HdY7c5sR3Ldye', 'L5MF2KSgPe', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(16, 'Abigale Greenfelder', 'seeker', 'waters.jermey@example.com', '2019-05-29 04:24:34', '$2y$10$vVTyeIR1mw5.FXHfTCryhui1Kh9Okmdluz3VD3oZOnT2fIq9m1JNi', 'JkkzJ7RThb', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(17, 'Nelson Schmeler', 'seeker', 'pkoelpin@example.com', '2019-05-29 04:24:34', '$2y$10$BGD/iel7Og8HwsmkE9xogeQ1o9vetatoKhxEA1xMfwFpotAwEHyWa', 'gdpSZrrlgK', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(18, 'Dr. Griffin Zieme', 'seeker', 'cody43@example.com', '2019-05-29 04:24:34', '$2y$10$8nsCzhMRRzpIv53snCIrPealG3INDA5bVtM6JMn51z0DssiLPO28y', 'VeBaiahjvE', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(19, 'Prof. Oliver Collins PhD', 'seeker', 'cblanda@example.org', '2019-05-29 04:24:34', '$2y$10$i65pyomOcqzOdhv5I/nFheh.ViLkPlFlkRg6evud0p.oU56JXlW3W', 'VSYwAc4fkm', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(20, 'Bertrand Krajcik', 'seeker', 'lester.block@example.net', '2019-05-29 04:24:34', '$2y$10$8ahDZ73CJ3ZaeY3ldQLk6uWKBmFBbsGOBClGDv2K6I2LZ0KPqj80.', 'gPbJjoKG9Z', '2019-05-29 04:24:35', '2019-05-29 04:24:35'),
(21, 'Teszt Elek', 'seeker', 'xyuser2test@gmail.com', NULL, '$2y$10$0bFhu6VlUyi01hJGjaxfyuLv/g9lnE4BmbbrQaS.rDKaSvSs5L0iW', '2Dz0vJd3fMAoB9Sn3qaBqU7BmFc4slqwK3uUkyJWo8y96J2w5CKOXXFMe2nw', '2019-05-29 04:39:55', '2019-05-29 04:39:55'),
(22, '', 'employer', 'xyeditor2test@gmail.com', NULL, '$2y$10$Y/lRg0Ifa9tkq4cueE4wUeXsWlXHUdm63Gn10CKb6MsznX3Rpl1Se', NULL, '2019-05-31 05:46:33', '2019-05-31 05:46:33');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `job_user`
--
ALTER TABLE `job_user`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- A tábla indexei `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT a táblához `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT a táblához `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT a táblához `job_user`
--
ALTER TABLE `job_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT a táblához `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT a táblához `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
