<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->

    <link href="{{ asset('css/portal.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/solid.css" integrity="sha384-QokYePQSOwpBDuhlHOsX0ymF6R/vLk/UQVz3WHa6wygxI5oGTmDTv8wahFOSspdm" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/fontawesome.css" integrity="sha384-vd1e11sR28tEK9YANUtpIOdjGW14pS87bUBuOIoBILVWLFnS+MCX9T6MMf0VdPGq" crossorigin="anonymous">




  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">


   @yield('add_to_head', View::make('headpart'))

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-primary navbar-laravel">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>

                               </li>
                                <li class="nav-item d-none d-md-block">
                                 <a class="nav-link">|</a>
                             </li>

                         <li class="nav-item">
                                <a class="nav-link" href="{{ route('employer.register') }}">{{ __('Employer Registration') }} (If you're hiring)</a>
                            </li>
                                <li class="nav-item d-none d-md-block">
                                 <a class="nav-link">|</a>
                             </li>



                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Job Seeker Registration') }} (If you're searching)</a>
                                </li>
                            @endif
                        @else

                            @if(Auth::user()->user_type == 'employer')

                            <li>
                                <a href="{{route('job.create')}}"><button class="btn btn-secondary">Post a Job</button></a>
                            </li>
                            @endif



                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                    @if(Auth::user()->user_type == 'employer')
                                        {{ Auth::user()->company->cname }}
                                     @else
                                        {{ Auth::user()->name }}
                                    @endif
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">


                                    @if(Auth::user()->user_type == 'employer')
                                    <a class="dropdown-item" href="{{ route('company.view') }}">
                                        {{ __('Company') }}
                                    </a>
                                     <a class="dropdown-item" href="{{ route('my.job')}}">
                                    MyJobs
                                    </a>
                                     <a class="dropdown-item" href="{{ route('applicant')}}">
                                   Applicants
                                    </a>
                                    @else
                                    <a class="dropdown-item" href="{{ route('profile.index')}}">
                                        {{ __('Profile') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('home')}}">
                                        {{ __('Saved Jobs') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('sent_applications')}}">
                                        {{ __('Sent Applications') }}
                                    </a>

                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Full Page Image Header with Vertically Centered Content -->
    <a  href="{{ url('/') }}"  style="color:#000;text-decoration:none;">
         <header class="imagehead  ">
          <div class="container ">
            <div class="row  align-items-center">
              <div class="col-12 text-center ">
                <h1 class="">PHP  Jobs</h1>
                 <p >Laravel, WordPress and Other PHP jobs</p>
              </div>
            </div>
          </div>
        </header>
    </a>

        <main class="py-4">
        
      <div class="container">
      <div class="row">
      <div class="col-md-12 p-4">
       <span style="font-weight:bold"> The Source Code: <a href="https://bitbucket.org/webeswork/laravel_vue_workportal/src/master/" target="_blank" >https://bitbucket.org/webeswork/laravel_vue_workportal</a>  </span>
      </div>
      </div>
      </div>
            @yield('content')
            
            
      <div class="container">
      <div class="row">
      <div class="col-md-12 p-4">
    <span style="font-weight:bold"> The Source Code: <a href="https://bitbucket.org/webeswork/laravel_vue_workportal/src/master/" target="_blank" >https://bitbucket.org/webeswork/laravel_vue_workportal</a>  </span>
      <br> <br>
      Contact : webeswork@yahoo.com<br>
      If you think this project is useful:<br>
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="3ZQVXW7CSEYKS" />
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_HU/i/scr/pixel.gif" width="1" height="1" />
</form>
      
      <br>
      <br>
      <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Laravel Workportal -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-9781487221982398"
     data-ad-slot="6130612741"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
      
      
      
      <br>
      <br>

      </div>
      </div>
      </div>      
        </main>
    </div>
</body>
</html>
