@extends('layouts.app')

@section('content')
<div class="container">
<div class="col-md-12">

            <div class="company-profile">

                  @if(!empty($company->cover_photo))
                  <div style="background-image: url({{asset('uploads/coverphoto/'.$company->cover_photo)}});background-size:cover;background-repeat: no-repeat;width:100%;height:100px;"   >
                       @if(empty($company->logo))
                       <img src="{{asset('avatar/company.png')}}"  style="width: 100px" />
                       @else
                       <img src="{{asset('uploads/logo/'.$company->logo)}}"  style="width: 100px;" />
                       @endif

                  </div>
                  @else
                       @if(empty($company->logo))
                       <img src="{{asset('avatar/company.png')}}"  style="width: 100px" />
                       @else
                       <img src="{{asset('uploads/logo/'.$company->logo)}}"  style="width: 100px;" />
                       @endif
                  @endif


                   <div class="company-desc"><br>

                      <h1> {{$company->cname}}</h1>
                      <p>
                      {{$company->description}}<br><br>

                      {{$company->slogan}}<br><br>
                      	Adress: {{$company->address}}<br>
                        Phone: {{$company->phone}}<br>
                        Website: {{$company->website}} </p>
                   </div>

            </div>


<table class="table" >
    <thead>
        <th>Position</th>
        <th>Address</th>
        <th>Date:</th>
        <th></th>
    </thead>
    <tbody>
        @foreach ($company->jobs as $job)
        <tr>
            <!--<td>
              <img src ="{{asset('avatar/man.jpg')}}" width="80" />
            </td>-->
            <td> {{$job->position}}
            <br>
            <i class="fa fa-clock" aria-hidden="true"></i>&nbsp; {{$job->type}}
            </td>
            <td><i class="fa fa-map-marker" aria-hidden="true"></i> {{$job->address}}</td>
            <td>
            <i class="fa fa-globe" aria-hidden="true"></i>
             {{$job->created_at->diffForHumans()}}</td>
            <td>
              <a href="{{route('jobs.show', [$job->id, $job->slug])}}" > <button class="btn btn-success btn-sm">Apply </button> </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>




</div>
</div>
@endsection
