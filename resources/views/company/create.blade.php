@extends('layouts.app')

@section('content')
<div class="container">

               @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif


    <div class="row">
    <div class="col-md-12 mb-2">

                   @if(!empty(Auth::user()->company->cover_photo))
                  <div style="background-image: url({{asset('uploads/coverphoto/'.Auth::user()->company->cover_photo)}});background-repeat: no-repeat;background-size:cover;width:100%;height:100px;"   >
                       @if(empty(Auth::user()->company->logo))
                       <img src="{{asset('avatar/company.png')}}"  style="width: 100px" />
                       @else
                       <img src="{{asset('uploads/logo/'.Auth::user()->company->logo)}}"  style="width: 100px;" />
                       @endif

                  </div>
                  @else
                       @if(empty(Auth::user()->company->logo))
                       <img src="{{asset('avatar/company.png')}}"  style="width: 100px" />
                       @else
                       <img src="{{asset('uploads/logo/'.Auth::user()->company->logo)}}"  style="width: 100px;" />
                       @endif
                  @endif

      </div>
    </div>
    <div class="row">

            <div class="col-md-3">

               @if(empty(Auth::user()->company->logo))
               <img src="{{asset('avatar/company.png')}}"  style="width: 100%" />
               @else
               <img src="{{asset('uploads/logo/'.Auth::user()->company->logo)}}"  style="width: 100%" />
               @endif


                <form action="{{route('company.logo')}}" method="POST"  enctype="multipart/form-data">

                    @csrf

                <div class="card">
                    <div class="card-header">Update logo</div>
                    <div class="card-body">
                        <input type="file" class="form-control" name="cover_logo" />
                        <br>

                        <button class="btn btn-dark float-right" type="submit">Update</button>
                    </div>

                </div>

                </form>


            </div>

             <div class="col-md-5">
               <div class="card">


                       <div class="card-header">Update Your Company Information</div>

                <form action="{{route('company.store')}}" method="POST" > @csrf

                       <div class="card-body">

                        <div class="form-group">
                            <label for="">Address</label>
                            <input type="text" class="form-control" name="address" value="{{Auth::user()->company->address}}" >
                       </div>

                       <div class="form-group">
                            <label for="">Phone</label>
                            <input type="text" class="form-control" name="phone" value="{{Auth::user()->company->phone}}" >
                       </div>


                       <div class="form-group">
                            <label for="">Website</label>
                            <input type="text" class="form-control" name="website" value="{{Auth::user()->company->website}}" >
                       </div>


                       <div class="form-group">
                            <label for="">Slogan</label>
                            <input type="text" class="form-control" name="slogan" value="{{Auth::user()->company->slogan}}" >
                       </div>


                       <div class="form-group">
                            <label for="">Description</label>
                           <textarea name="description"  class="form-control" >{{Auth::user()->company->description}}</textarea>
                       </div>





                       <div class="form-group">
                            <button class="btn btn-dark" type="submit">Update</button>
                       </div>


                       @if(Session::has('message'))
                       <div class="alert alert-success">
                               {{Session::get('message')}}
                       </div>
                       @endif

                       </div>

                   </form>

                </div>
            </div>

             <div class="col-md-4">





                <div class="card">
                    <div class="card-header">About your company</div>
                    <div class="card-body">
                      <p>Company Name: {{Auth::user()->company->cname}}</p>
                      <p>Address: {{Auth::user()->company->address}}</p>
                      <p>Phone: {{Auth::user()->company->phone}}</p>
                      <p>Website: {{Auth::user()->company->website}}</p>
                      <p>Slogan: {{Auth::user()->company->slogan}}</p>
                       <p>Description: {{Auth::user()->company->description}}</p>

                      <p> Company page: <a href="{{ url('company/'.Auth::user()->company->id.'/'.Auth::user()->company->slug)}}">View</a></p>





                    </div>
                </div>



                <form action="{{route('cover.photo')}}" method="POST"  enctype="multipart/form-data">

                    @csrf

                <div class="card">
                    <div class="card-header">Update cover photo</div>
                    <div class="card-body">
                        <input type="file" class="form-control" name="cover_photo" />
                        <br>

                        <button class="btn btn-dark float-right" type="submit">Update</button>
                    </div>

                </div>

                </form>








            </div>


</div>
</div>
@endsection
