@extends('layouts.app')

@section('content')

<div class="container" >



      @if(!empty($job->company->cover_photo))

       <h3>{{$job->company->cname}}</h3>
      <div style="background-image: url({{asset('uploads/coverphoto/'.$job->company->cover_photo)}});background-size:cover;background-repeat:no-repeat;"   >
           @if(empty($job->company->logo))
           <img src="{{asset('avatar/company.png')}}"  style="width: 100px" />
           @else
           <img src="{{asset('uploads/logo/'.$job->company->logo)}}"  style="width: 100px;" />
           @endif

      </div>
      @else
           @if(empty($job->company->logo))
           <img src="{{asset('avatar/company.png')}}"  style="width: 100px" />
           @else
           <img src="{{asset('uploads/logo/'.$job->company->logo)}}"  style="width: 100px;" />
           @endif
      @endif



</div>




<div class="container">
                         @if(Session::has('message'))
                       <div class="alert alert-success">
                               {{Session::get('message')}}
                       </div>
                       @endif
    <div class="row ">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$job->title}}</div>

                <div class="card-body">
                      <h4>Description:</h4>
                      <p>{{$job->description}}</p>

                      <div>
                       <h4>Role:</h4>
                      <p>{{$job->roles}}</p>
                       </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
             <div class="card">
                <div class="card-header">Short Info</div>

                <div class="card-body">

<p>Company:  <a href="{{route('company.index', [$job->company->id, $job->company->slug])}}">{{$job->company->cname }} </a></p>
<p>Address: {{$job->address}} </p>
<p>Employment Type: {{$job->type}} </p>
<p>Position:   {{$job->position}} </p>
<p>Posted:  {{$job->created_at->diffForHumans() }} </p>
<p>Last date to apply:  {{date('F d, Y', strtotime($job->last_date)) }} </p>


                </div>
            </div>


            <br>
            @if(Auth::check() && Auth::user()->user_type =="seeker")
              @if(!$job->checkApplication())
              <!--  <form action="{{route('apply', [$job->id])}}" method="POST"> @csrf
            <button type="submit" class="btn btn-success" style="width: 100%"  >Apply </button>
              </form>-->

             <apply-component  jobid ="{{$job->id}}"  app_url ="{{url('/')}}" ></apply-component>

              @else
              Application  sent!
              <br>
              @endif
              <br>
                 <favorite-component  jobid ="{{$job->id}}"  app_url ="{{url('/')}}"  :favorited={{$job->checkSaved() ? 'true' : 'false'}}></favorite-component>
              @else

            <a class="nav-link" href="{{ route('login') }}"><button type="button" class="btn btn-success" style="width: 100%"  >Apply </button></a>
            @endif

        </div>




    </div>
</div>
@endsection
