
@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
  <div class="col-md-12">
    <search-component app_url ="{{url('/')}}" categories_data="{{App\Category::all()}}"></search-component>
      </div>
        <!--
  <form action="{{route('alljobs')}}" method="GET">

    <div class="form-inline">
        <div class="form-group">
            <label>Keyword: &nbsp;</label>
            <input type="text" name="title" class="form-control">&nbsp;&nbsp;&nbsp;
        </div>
        <div class="form-group">
            <label>Employment type: &nbsp;</label>
            <select class="form-control" name="type">
                    <option value="">-select-</option>
                    <option value="fulltime">fulltime</option>
                    <option value="parttime">parttime</option>
                    <option value="casual">casual</option>
                </select>
                &nbsp;&nbsp;
        </div>
        <div class="form-group">
            <label> Category: &nbsp;</label>
            <select name="category_id" class="form-control">
                <option value="">-select-</option>

                    @foreach(App\Category::all() as $cat)
                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                    @endforeach
                </select>
                &nbsp;&nbsp;
        </div>
--
        <div class="form-group">
            <label>address</label>
            <input type="text" name="address" class="form-control">&nbsp;&nbsp;
        </div>
--
        <div class="form-group">
            <button type="submit" class="btn btn-outline-success">Search</button>
        </div>
    </div>

</form>
-->

    <br><br>
</div>
<br>
@foreach ($jobs as $job)
<div class="row mb-3" style="border-bottom: 1px solid #ccc;padding-bottom:20px">
      <div class="col-sm-3 col-xs-6">
        <img src ="{{asset('uploads/logo/')}}/{{$job->company->logo}}" width="80" />
      </div>

    <div class="col-sm-3 col-xs-6">
        Category: {{$job->category->name}}
    <br>
    <i class="fa fa-clock" aria-hidden="true"></i>&nbsp; {{$job->type}}
    </div>
    <div class="col-sm-3 col-xs-6">
      Position: {{$job->position}}<br>
    <i class="fa fa-map-marker" aria-hidden="true"></i>Address: {{$job->address}}<br>
    <i class="fa fa-globe" aria-hidden="true"></i>
    Date: {{$job->created_at->diffForHumans()}}
    </div>

    <div class="col-sm-3 col-xs-6">
      <a href="{{route('jobs.show', [$job->id, $job->slug])}}" > <button class="btn btn-success ">Show </button> </a>
  </div>
</div>
@endforeach


{{$jobs->appends(Illuminate\Support\Facades\Input::except('page'))->links()}}


    </div>









@endsection

<style>

.fa{
    color: #4183D7;
}

</style>
