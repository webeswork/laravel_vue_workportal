@extends('layouts.app')

@section('add_to_head')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <script  defer src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">



     <!-- Scripts -->
    <script>
    $( function() {
    $( "#datepicker" ).datepicker();
    } );
    </script>
@endsection


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">

                <div class="card-header">Create a job</div>

            <div class="card-body">

            <form action="{{route('job.store')}}" method="POST">

                @csrf

            <div class="form-group">
                <label for ="title"> Title:  </label>
                <input type="text" name = "title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ old('title') }}">
                   @if ($errors->has('title'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('title') }}</strong>
                      </span>
                  @endif

            </div>

            <div class="form-group ">
                <label for ="description"> Description:  </label>
                <textarea  name = "description" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" >{{ old('description') }}</textarea>

                  @if ($errors->has('description'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                  @endif

            </div>

            <div class="form-group">
                <label for ="roles"> Role:  </label>
                <textarea  name = "roles" class="form-control {{ $errors->has('roles') ? ' is-invalid' : '' }}" >{{ old('roles') }}</textarea>
                  @if ($errors->has('roles'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('roles') }}</strong>
                      </span>
                  @endif
            </div>

            <div class="form-group">
                <label for ="category"> Category:  </label>
                <select name ="category" class="form-control">
                        @foreach(App\Category::all() as $cat)
                          <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                </select>

            </div>

            <div class="form-group">
                <label for ="position"> Position:  </label>
                <input type="text" name = "position" class="form-control {{ $errors->has('position') ? ' is-invalid' : '' }}" value="{{ old('position') }}">
                  @if ($errors->has('position'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('position') }}</strong>
                      </span>
                  @endif
            </div>

            <div class="form-group">
                <label for ="address"> Address:  </label>
                <input type="text" name = "address" class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" value="{{ old('address') }}">
                @if ($errors->has('address'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('address') }}</strong>
                      </span>
                  @endif
            </div>


             <div class="form-group">
                <label for ="type"> Type:  </label>
                 <select name ="type" class="form-control">
                          <option value="fulltime">fulltime</option>
                          <option value="parttime">parttime</option>
                          <option value="casual">casual</option>
                </select>
            </div>

           <div class="form-group">
                <label for ="status"> Status:  </label>
                 <select name ="status" class="form-control">
                          <option value="1">live</option>
                          <option value="0">draft</option>
                </select>
            </div>



            <div class="form-group">
                <label for ="last_date"> Last date:  </label>
                <input type="text" name = "last_date"  id="datepicker" class="form-control {{ $errors->has('last_date') ? ' is-invalid' : '' }}" value="{{ old('last_date') }}">

                   @if ($errors->has('last_date'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('last_date') }}</strong>
                      </span>
                  @endif
            </div>

              <datepicker-component></datepicker-component>

             <div class="form-group">
              <button type="submit" class="btn btn-dark">Submit</button>
            </div>

                       @if(Session::has('message'))
                       <div class="alert alert-success">
                               {{Session::get('message')}}
                       </div>
                       @endif


               </form>

            </div>

             </div>
        </div>
    </div>
</div>
@endsection
