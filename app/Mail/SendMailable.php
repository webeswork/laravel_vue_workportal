<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $c_name;
    public $c_mail;
    public $applicant_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($c_name, $c_mail, $applicant_name)
    {
        $this->c_name = $c_name;
        $this->c_mail = $c_mail;
        $this->applicant_name = $applicant_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->view('email.application');
    }
}
