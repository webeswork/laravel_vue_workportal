<?php

namespace App\Http\Controllers;

use App\Profile;
use File;
use Illuminate\Http\Request;
use Image;
use Validator;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('seeker');
    }

    public function index()
    {
        return view('profile.index');
    }

    public function store(Request $request)
    {
        $user_id = auth()->user()->id;
        Profile::where('user_id', $user_id)->update([
            'address' => request('address'),
            'experience' => request('experience'),
            'bio' => request('bio'),
        ]);

        return redirect()->back()->with('message', 'Profile Successfully Updated!');
    }

    public function coverletter(Request $request)
    {

        //if older  exists than delete it
        if (!empty(\Auth::user()->profile->cover_letter)) {
            \Storage::delete(\Auth::user()->profile->cover_letter);
        }

        $file_size = env('FILE_SIZE');

        //no spaces between file types!!

        $rules['cover_letter'] = 'required|image|mimes:JPG,png,jpg,jpeg,pdf,txt,docx,doc|max:' . $file_size;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }

        $user_id = auth()->user()->id;

        $cover = $request->file('cover_letter')->store('public/files');

        Profile::where('user_id', $user_id)->update([
            'cover_letter' => $cover,

        ]);

        return redirect()->back()->with('message', 'Cover letter Successfully Updated!');

    }

    public function resume(Request $request)
    {

        //if older  exists than delete it
        if (!empty(\Auth::user()->profile->resume)) {
            \Storage::delete(\Auth::user()->profile->resume);
        }

        // echo $request->file('resume')->getClientOriginalExtension();

        $file_size = env('FILE_SIZE');

        //no spaces between file types!!

        $rules['resume'] = 'required|image|mimes:JPG,png,jpg,jpeg,pdf,txt,docx,doc|max:' . $file_size;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }

        $user_id = auth()->user()->id;

        $resume = $request->file('resume')->store('public/files');

        Profile::where('user_id', $user_id)->update([
            'resume' => $resume,

        ]);

        return redirect()->back()->with('message', 'Resume Successfully Updated!');

    }

    public function avatar(Request $request)
    {

        //image size
        $image_size = env('IMAGE_SIZE');
        $rules['avatar'] = 'required|image|mimes:png,jpg,jpeg|max:' . $image_size;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }

        //if older avatar exists than delete it

        if (!empty(\Auth::user()->profile->avatar)) {
            File::delete('uploads/avatar/' . \Auth::user()->profile->avatar);
        }

        $user_id = auth()->user()->id;

        if ($request->hasfile('avatar')) {
            $file = $request->file('avatar');
            $ext = $file->getClientOriginalExtension();
            $new_filename = 'avatar_' . time() . '.' . $ext;
            $filename = $file->getClientOriginalName();

            $destinationPath = public_path() . '/pictures/';

            $file->move('uploads/avatar/', $new_filename);

            $img = Image::make('uploads/avatar/' . $new_filename);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save('uploads/avatar/' . $new_filename);
            //File::delete('uploads/avatar/' . $filename);

            Profile::where('user_id', $user_id)->update([
                'avatar' => $new_filename,

            ]);

            return redirect()->back()->with('message', 'Profile picture Successfully Updated!');

        }

    }

}
