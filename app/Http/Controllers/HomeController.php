<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $jobs = \Auth::user()->favorites;
        //var_dump($jobs);
        return view('home', compact('jobs'));
    }

    public function sentApplications()
    {

        $jobs = \Auth::user()->applications;
        //var_dump($jobs);
        return view('home', compact('jobs'));
    }

    public function test()
    {

        return view('test');
    }
}
