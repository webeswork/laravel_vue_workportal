<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\JobPostRequest;
use App\Job;
use App\Mail\SendMailable;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class JobController extends Controller
{

    public function __construct()
    {
        $this->middleware('employer', ['except' => array('index', 'show', 'apply', 'allJobs', 'searchJobs', 'searchInCategory')]);
    }

    public function index()
    {

        //$categories = \App\Category::all();
        //var_dump($categories);

        $jobs = Job::latest()->limit(10)->where('status', 1)->get();
        //$jobs = Job::all();
        // $companies = Company::latest()->limit(12)->get();
        $companies = Company::get()->random(12);
        return view('welcome', compact('jobs', 'companies'));

    }

//Clean: Job $job  similar than  $job= Job::find($id);
    public function show($id, Job $job)
    {

        //dd($job->id);
        return view('jobs.show', compact('job'));
    }

    public function myjob()
    {
        $jobs = Job::where('user_id', auth()->user()->id)->get();

        return view('jobs.myjob', compact('jobs'));

    }

    public function edit($id)
    {
        $job = Job::findOrFail($id);
        return view('jobs.edit', compact('job'));

    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $job = Job::findOrFail($id);
        $job->update($request->all());

        return redirect()->back()->with('message', 'Job Successfully Updated!');

    }

    public function applicant()
    {
        $applicants = Job::has('users')->where('user_id', auth()->user()->id)->get();

        // var_dump($applicants);

        return view('jobs.applicants', compact('applicants'));

    }

    public function create()
    {
        return view('jobs.create');
    }

    public function store(JobPostRequest $request)
    {
        $user_id = auth()->user()->id;
        $company = Company::where('user_id', $user_id)->first();
        $company_id = $company->id;

        Job::create([
            'user_id' => $user_id,
            'company_id' => $company_id,
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'description' => request('description'),
            'roles' => request('roles'),
            'category_id' => request('category'),
            'position' => request('position'),
            'address' => request('address'),
            'type' => request('type'),
            'status' => request('status'),
            'last_date' => request('last_date'),

        ]);

        return redirect()->back()->with('message', 'Job posted  successfully!');

    }

    public function apply(Request $request, $id)
    {
        $jobId = Job::find($id);

//adat az emailhez
        $c_name = $jobId->company->cname . "<br>";

        $c_mail = $jobId->company->user->email . "<br>";

        $applicant_name = Auth::user()->name;

        try {

            Mail::to($c_mail)->cc('webeswork@yahoo.com')->send(new SendMailable($c_name, $c_mail, $applicant_name));

        } catch (\Exception $e) {

            $error_message = $e->getMessage();
            echo $error_message;
        }

        //To attach a role to a user by inserting a record in the intermediate table that joins the models, use the attach method
        $jobId->users()->attach(Auth::user()->id);
        //attach  https://laravel.com/docs/5.8/eloquent-relationships

        //email értesítés küldés

        return redirect()->back()->with('message', 'Application  sent!')->withErrors($error_message);

    }

    public function allJobs(Request $request)
    {

        $keyword = $request->get('title');
        $type = $request->get('type');
        $category = $request->get('category_id');
        $address = $request->get('address');
        if ($keyword) {
            $jobs = Job::where('title', 'LIKE', '%' . $keyword . '%')
                ->orWhere('description', 'LIKE', '%' . $keyword . '%')
                ->orWhere('position', 'LIKE', '%' . $keyword . '%')
                ->paginate(10);
        } else if ($type) {
            $jobs = Job::orWhere('type', $type)
                ->paginate(10);
        } else if ($category) {
            $jobs = Job::orWhere('category_id', $category)
                ->paginate(10);

        } else {
            $jobs = Job::latest()->paginate(10);
        }

        return view('jobs.alljobs', compact('jobs'));

    }

    public function searchInCategory($id)
    {

        // echo "category_id:" . $id;

        if ($id) {
            $jobs = Job::where('category_id', $id)
                ->paginate(10);

        }

        return view('jobs.alljobs', compact('jobs'));

    }

    public function searchJobs(Request $request)
    {
        $keyword = $request->get('keyword');
        $job = Job::where('title', 'like', '%' . $keyword . '%')
            ->orWhere('position', 'like', '%' . $keyword . '%')
            ->limit(5)->get();

        return response()->json($job);
    }

}
