<?php

namespace App\Http\Controllers;

use App\Company;
use File;
use Illuminate\Http\Request;
use Image;
use Validator;

class CompanyController extends Controller
{

    public function __construct()
    {
        $this->middleware('employer', ['except' => array('index')]);
    }

    public function index($id, Company $company)
    {

        // $jobs = Job::where('user_id', $id)->get();

        return view('company.index', compact('company'));
    }

    public function create()
    {

        return view('company.create');
    }

    public function store(Request $request)
    {
        $user_id = auth()->user()->id;

        Company::where('user_id', $user_id)->update([
            'address' => request('address'),
            'phone' => request('phone'),
            'website' => request('website'),
            'slogan' => request('slogan'),
            'description' => request('description'),
        ]);

        return redirect()->back()->with('message', 'Company Successfully Updated!');
    }

    public function coverPhoto(Request $request)
    {

        //image size
        $image_size = env('IMAGE_SIZE');
        $rules['cover_photo'] = 'required|image|mimes:png,jpg,jpeg|max:' . $image_size;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }

        if (!empty(\Auth::user()->company->logo)) {
            File::delete('uploads/coverphoto/' . \Auth::user()->company->cover_photo);
        }

        $user_id = auth()->user()->id;

        if ($request->hasfile('cover_photo')) {

            $file = $request->file('cover_photo');

            $ext = $file->getClientOriginalExtension();

            $filename = time() . '.' . $ext;

            $file->move('uploads/coverphoto/', $filename);

            $img = Image::make('uploads/coverphoto/' . $filename);
            $img->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save('uploads/coverphoto/' . $filename);

            Company::where('user_id', $user_id)->update([
                'cover_photo' => $filename,

            ]);

            return redirect()->back()->with('message', 'Company Cover Photo  Successfully Updated!');

        }
    }

    public function companyLogo(Request $request)
    {

        //image size
        $image_size = env('IMAGE_SIZE');
        $rules['cover_logo'] = 'required|image|mimes:png,jpg,jpeg|max:' . $image_size;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }

        //if older avatar exists than delete it

        if (!empty(\Auth::user()->company->logo)) {
            File::delete('uploads/logo/' . \Auth::user()->company->logo);
        }

        $user_id = auth()->user()->id;

        if ($request->hasfile('cover_logo')) {

            $file = $request->file('cover_logo');

            $ext = $file->getClientOriginalExtension();

            $filename = time() . '.' . $ext;

            $file->move('uploads/logo/', $filename);

            $img = Image::make('uploads/logo/' . $filename);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save('uploads/logo/' . $filename);

            Company::where('user_id', $user_id)->update([
                'logo' => $filename,

            ]);

            return redirect()->back()->with('message', 'Company Logo Successfully Updated!');

        }

    }

}
