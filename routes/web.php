<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/*Route::get('/', function () {
return view('welcome');
});*/

Route::get('/', 'JobController@index');

Auth::routes();
//email verified
//Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/sent_applications', 'HomeController@sentApplications')->name('sent_applications');

Route::get('/test', 'HomeController@test')->name('test');

Route::get('/job/{id}/{job}', 'JobController@show')->name('jobs.show');

Route::get('/company/{id}/{company}', 'CompanyController@index')->name('company.index');

Route::get('company/create', 'CompanyController@create')->name('company.view');

Route::post('company/create', 'CompanyController@store')->name('company.store');

Route::post('company/coverphoto', 'CompanyController@coverPhoto')->name('cover.photo');

Route::post('company/logo', 'CompanyController@companyLogo')->name('company.logo');

//user profile

Route::get('user/profile', 'UserController@index')->name('profile.index');

Route::post('user/profile/create', 'UserController@store')->name('profile.create');

Route::post('user/coverletter', 'UserController@coverletter')->name('cover.letter');

Route::post('user/resume', 'UserController@resume')->name('resume');

Route::post('user/avatar', 'UserController@avatar')->name('avatar');

Route::view('employer/register', 'auth.employer-register')->name('employer.register');

Route::post('employer/register', 'EmployerRegisterController@employerRegister')->name('emp.register');

//jobs
Route::get('/', 'JobController@index');
Route::get('/jobs/create', 'JobController@create')->name('job.create');
Route::post('/jobs/store', 'JobController@store')->name('job.store');
Route::get('/jobs/{id}/edit', 'JobController@edit')->name('job.edit');
Route::post('/jobs/{id}/edit', 'JobController@update')->name('job.update');
Route::get('/jobs/my-job', 'JobController@myjob')->name('my.job');

Route::post('/applications/{id}', 'JobController@apply')->name('apply');

Route::get('/jobs/applications', 'JobController@applicant')->name('applicant');

Route::get('/jobs/alljobs', 'JobController@allJobs')->name('alljobs');

Route::get('/category/{id}', 'JobController@searchInCategory');

//save and unsave job

Route::post('/save/{id}', 'FavouriteController@saveJob');

Route::post('/unsave/{id}', 'FavouriteController@unsaveJob');

//Search
Route::get('/jobs/search', 'JobController@searchJobs');
