<?php

use App\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'user_type' => 'seeker',
        'email_verified_at' => now(),
        'password' => bcrypt('12345678'), // pass:12345678
        'remember_token' => Str::random(10),
    ];
});

//image faker: https://github.com/fzaninotto/Faker/blob/master/src/Faker/Provider/Image.php

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'user_id' => App\User::all()->random()->id,
        'cname' => $name = $faker->company,
        'slug' => str_slug($name),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'website' => $faker->domainName,
        'logo' => $faker->image(public_path('uploads/logo'), 100, 100, 'abstract', false),
        'cover_photo' => $faker->image(public_path('uploads/coverphoto'), 400, 100, 'business', false),
        'slogan' => 'lorem ipsum',
        'description' => $faker->paragraph(rand(2, 10)),

    ];
});

$factory->define(App\Job::class, function (Faker $faker) {
    return [
        'user_id' => App\User::all()->random()->id,
        'company_id' => App\Company::all()->random()->id,
        'title' => $title = $faker->text,
        'slug' => str_slug($title),
        'position' => $faker->jobTitle,
        'address' => $faker->address,
        'category_id' => rand(1, 5),
        'type' => 'fulltime',
        'status' => rand(0, 1),
        'description' => $faker->paragraph(rand(2, 10)),
        'roles' => $faker->text,
        'last_date' => $faker->DateTime,

    ];
});
